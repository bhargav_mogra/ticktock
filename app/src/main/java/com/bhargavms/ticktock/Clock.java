package com.bhargavms.ticktock;

import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import java.util.Calendar;


/**
 * A view that shows an analog clock.
 */
public class Clock extends View {
    private Drawable mClockDrawable;
    private Drawable mMinDrawable;
    private Drawable mSecondDrawable;
    private Drawable mHourDrawable;

    private float hourF;
    private float minF;
    private int second;

    private ValueAnimator mAnimator;

    public Clock(Context context) {
        super(context);
        init();
    }

    public Clock(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public Clock(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @SuppressWarnings("unused")
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public Clock(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    @SuppressWarnings("deprecation")
    private void init() {
        mClockDrawable = getResources().getDrawable(R.drawable.clock);
        mMinDrawable = getResources().getDrawable(R.drawable.min);
        mHourDrawable = getResources().getDrawable(R.drawable.hour);
        mSecondDrawable = getResources().getDrawable(R.drawable.second);

        mAnimator = ValueAnimator.ofInt(0, 60);
        mAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                Calendar now = Calendar.getInstance();
                int hour = now.get(Calendar.HOUR_OF_DAY);
                int min = now.get(Calendar.MINUTE);
                second = now.get(Calendar.SECOND);

                hourF = ((hour + ((float) min / 60f)) % 12) * 5;
                minF = min + ((float) second / 60f);
                invalidate();
                Log.v("time", "hour of day = " + hour);
                Log.v("time", "minute = " + min);
            }
        });
        mAnimator.setRepeatCount(ValueAnimator.INFINITE);
        post(new Runnable() {
            @Override
            public void run() {
                mAnimator.start();
            }
        });
    }

    @Override
    protected void onDraw(Canvas canvas) {
        int left = 0;
        int right = getWidth();
        int top = 0;
        int bottom = getHeight();
        mClockDrawable.setBounds(left, top, right, bottom);
        mHourDrawable.setBounds(left, top, right, bottom);
        mSecondDrawable.setBounds(left, top, right, bottom);
        mMinDrawable.setBounds(left, top, right, bottom);

        mClockDrawable.draw(canvas);

        int midX = getWidth() / 2;
        int midY = getHeight() / 2;

        canvas.save();
        canvas.rotate(hourF * 6, midX, midY);
        mHourDrawable.draw(canvas);
        canvas.restore();

        canvas.save();
        canvas.rotate(minF * 6, midX, midY);
        mMinDrawable.draw(canvas);
        canvas.restore();

        canvas.save();
        canvas.rotate(second * 6, midX, midY);
        mSecondDrawable.draw(canvas);
        canvas.restore();
    }
}
